# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import PoolMeta


class CostAllocationPatternCombinedLocation(ModelView, ModelSQL):
    """Cost allocation pattern - Combined location"""
    __name__ = 'cost.manage.cost.allocation.pattern-stock.combined.location'

    _table = 'cost_manage_cost_allocation_pattern_combined_rel'

    pattern = fields.Many2One('cost.manage.cost.allocation.pattern', 'Pattern',
        required=True, select=True, ondelete='CASCADE')
    location = fields.Many2One('stock.location.combined', 'Location',
        required=True, select=True, ondelete='CASCADE')


class CostAllocationPattern(metaclass=PoolMeta):
    __name__ = 'cost.manage.cost.allocation.pattern'

    combined_locations = fields.Many2Many(
        'cost.manage.cost.allocation.pattern-stock.combined.location',
        'pattern', 'location', 'Combined locations')
